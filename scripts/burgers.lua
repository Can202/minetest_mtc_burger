
--Burgers

--Classic Burger
minetest.register_craftitem("mtc_burger:classic_burger", {
	description = ("Classic Burger"), --nombre
	inventory_image = "mtc_burger_classic_burger.png", --poner textura
	on_use = minetest.item_eat (14) --comer
})
minetest.register_craft({
	output = "mtc_burger:classic_burger",
	recipe = {
		{"","farming:bread",""},
		{"farming:lettuce","mobs:meat","farming:tomato"},
		{"","farming:bread",""},
	}
})

--Vegetarian Burger
minetest.register_craftitem("mtc_burger:vegetarian_burger", {

	description = ("Vegetarian Burger"), --nombre
	inventory_image = "mtc_burger_vegetarian_burger.png", --poner textura
	on_use = minetest.item_eat (12) --comer

})
minetest.register_craft({
	output = "mtc_burger:vegetarian_burger",
	recipe = {
		{"","farming:bread",""},
		{"farming:lettuce","","farming:tomato"},
		{"","farming:bread",""},
	}
})

--Burger with Mayonnaise
minetest.register_craftitem("mtc_burger:burger_with_mayonnaise", {

	description = ("Burger with Mayonnaise"), --nombre
	inventory_image = "mtc_burger_burger_with_mayonnaise.png", --poner textura
	on_use = minetest.item_eat (14) --comer

})
minetest.register_craft({
	output = "mtc_burger:burger_with_mayonnaise",
	recipe = {
		{"group:food_egg","farming:bread","group:food_egg"},
		{"farming:lettuce","mobs:meat","farming:tomato"},
		{"","farming:bread",""},
	}
})
--Burger with Mustard
minetest.register_craftitem("mtc_burger:burger_with_mustard", {

	description = ("Burger with Mustard"), --nombre
	inventory_image = "mtc_burger_burger_with_mustard.png", --poner textura
	on_use = minetest.item_eat (14) --comer

})
minetest.register_craft({
	output = "mtc_burger:burger_with_mustard",
	recipe = {
		{"farming:seed_wheat","farming:bread","farming:seed_wheat"},
		{"farming:lettuce","mobs:meat","farming:tomato"},
		{"","farming:bread",""},
	}
})

--Cheeseburger
minetest.register_craftitem("mtc_burger:cheeseburger", {

	description = ("Cheeseburger"), --nombre
	inventory_image = "mtc_burger_cheeseburger.png", --poner textura
	on_use = minetest.item_eat (10) --comer

})
minetest.register_craft({
	output = "mtc_burger:cheeseburger",
	recipe = {
		{"mobs:cheese","farming:bread","mobs:cheese"},
		{"","mobs:meat",""},
		{"","farming:bread",""},
	}
})